# Social Media Sync

This script aggregates my Fediverse posts, then forward them to my Telegram channel and Telegram groups. It also forwards text-only posts to Twitter.

Reposts are sent only to Telegram channel, and only as links.

Polls are not forwarded.

Posts with sensitive flag or spoiler text are sent to Telegram with proper spoiler hiding.

This script was originally for forwarding Twitter posts, but Elon Musk fucked Twitter up (stops API and actively combats scraping).

## Usage

1. Setup configuration options, see section below.
2. `bundle install`
3. `bin/social-media-sync twitter-auth`
4. Configure repeated execution of `bin/social-media-sync`.

## Configuration Options

Configurable via environment variables, `.env` files, and `key=value` command line arguments.

`STATE_DIR`: Directory to store the state of the program

`MASTODON_URL`: Root URL of the Mastodon instance

`MASTODON_USERNAME`: Mastodon username, including instance domain

`MASTODON_CLIENT_ID`, `MASTODON_CLIENT_SECRET`, `MASTODON_ACCESS_TOKEN`: OAuth shenanigans obtained via `scripts/authorize-mastodon.sh`

`TELEGRAM_TOKEN`: Telegram bot token

`TELEGRAM_USERNAME`: Username of the Telegram bot

`TELEGRAM_CHAT_ID`: Chat ID of the channel to forward message to

`TELEGRAM_FORWARD_CHAT_IDS` (optional): Additional chat IDs to forward the forwarded channel messages to, separated by commas

`TELEGRAM_DISCUSSION_INVITE_LINK` (optional): Invite link to a discussion group of forwarded messages

`TWITTER_USERNAME`: Twitter username, excluding "@"

`TWITTER_CLIENT_ID`, `TWITTER_CLIENT_SECRET`: OAuth shenanigans obtained by registering for Twitter API and set up for OAuth 2.0