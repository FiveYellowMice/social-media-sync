require 'date'
require 'sanitize'
require 'social_media_sync/cli_parser'
require 'social_media_sync/config_loader'
require 'social_media_sync/persistent_data'
require 'social_media_sync/mastodon'
require 'social_media_sync/telegram'
require 'social_media_sync/twitter'

class SocialMediaSync
  
  def self.run
    command = SocialMediaSync::CLIParser.parse
    SocialMediaSync::ConfigLoader.load

    case command
    when 'twitter-auth'
      SocialMediaSync::Twitter.new.initial_authorize do |auth_url|
        puts "Open this URL: " + auth_url
        print "Enter the code parameter in the callback URL: "
        STDIN.gets.chomp
      end
      puts "Success!"
    when 'sync'
      SocialMediaSync.new.run
    end
  end

  include PersistentData
  persistent_data_accessor :last_synced_status_time, "last_synced_status_time.txt", DateTime.new(0),
    ->(t) { DateTime.iso8601(t.chomp) },
    ->(v) { v.iso8601 + "\n" }

  def initialize
    @telegram = SocialMediaSync::Telegram.new
    @mastodon = SocialMediaSync::Mastodon.new
    @twitter = SocialMediaSync::Twitter.new
  end

  def run
    # Get posts from Mastodon, sort by time, and filter out posts that have already been synced
    mastodon_statuses = @mastodon.get("/api/v1/accounts/#{ENV['MASTODON_USERNAME']}/statuses", {
      exclude_replies: true,
    })
    mastodon_statuses.each do |status|
      status['created_at'] = DateTime.iso8601(status['created_at']) if status['created_at']
      status['edited_at'] = DateTime.iso8601(status['edited_at']) if status['edited_at']
    end
    mastodon_statuses.sort_by!{|status| status['created_at'] }
    mastodon_statuses.select!{|status| status['created_at'] > last_synced_status_time }

    mastodon_statuses.each do |status|
      next unless ['public', 'unlisted'].include?(status['visibility'])
      next if status['poll']

      puts "Syncing status #{status['url']}"

      if status['reblog']
        # For reblogs, just send the URL of the original post, also don't forward them
        @telegram.send_message(
          chat_id: ENV['TELEGRAM_CHAT_ID'],
          text: status['reblog']['url'],
          disable_notification: false,
        )
      
      # Do the full business for own posts
      else

        # Convert HTML to plain text for Twitter
        whitespace_elements = (
          %w[p div h1 h2 h3 h4 h5 h6].map{|tag| [tag, {before: "\n", after: "\n"}] } +
          %w[br ul ol].map{|tag| [tag, {before: "\n", after: ""}] } +
          %w[li].map{|tag| [tag, {before: "• ", after: "\n"}] }
        ).to_h
        twitter_content = Sanitize.fragment(status['content'],
          elements: [],
          whitespace_elements: whitespace_elements,
          transformers: [
            proc {|input|
              if !input[:is_allowlisted] && input[:node_name] == 'a'
                input[:node].name = 'span'
                if input[:node]['href'] != input[:node].content
                  input[:node].content = input[:node].content + " (#{input[:node]['href']})"
                end
                input[:node].remove_attribute('href')
              end
            }
          ],
        )
        twitter_content = CGI.unescapeHTML(twitter_content)

        # Send to Twitter, ignoring errors
        twitter_status_id = nil
        begin
          if !status['media_attachments'].empty?
            raise Twitter::Error, "media upload is not supported via APIv2"
          end
          response = @twitter.post('tweets', text: twitter_content)
          twitter_status_id = response['data']['id']
        rescue Twitter::Error => err
          puts "Cannot post to Twitter, error: #{err.message}"
        end

        # Sanitize HTML to Telegram-safe pseudo-HTML
        telegram_content = Sanitize.fragment(status['content'],
          elements: %w[b strong i em u ins s strike del a code pre],
          attributes: {'a' => ['href']},
          protocols: {'a' => {'href' => ['http', 'https']}},
          whitespace_elements: whitespace_elements,
        )

        # Handle sensitive content and spoilers
        hide_images = status['sensitive'] || false

        if status['spoiler_text'] && !status['spoiler_text'].empty?
          telegram_content = "#{html_escape status['spoiler_text']}\n\n<tg-spoiler>#{telegram_content}</tg-spoiler>"
          hide_images = true
        end

        # Prepare an inline keyboard that links to the original post, and Twitter post (if sent)
        reply_markup = {
          inline_keyboard: [
            [
              {
                text: 'Read/Comment on Fediverse',
                url: status['url'],
              }
            ],
            ([
              {
                text: 'Read/Comment on Twitter',
                url: "https://twitter.com/#{ENV['TWITTER_USERNAME']}/status/#{twitter_status_id}",
              },
            ] if twitter_status_id),
            ([
              {
                text: 'Comment on Telegram',
                url: ENV['TELEGRAM_DISCUSSION_INVITE_LINK'],
              }
            ] if ENV['TELEGRAM_DISCUSSION_INVITE_LINK'] && !ENV['TELEGRAM_DISCUSSION_INVITE_LINK'].empty?),
          ].delete_if(&:nil?)
        }

        # Send to Telegram, only images and videos are supported
        sent_message_id = nil
        media = status['media_attachments'].select{|media| %w[image video gifv].include?(media['type']) }
        case media.length
        when 0
          sent_message_id = @telegram.send_message(
            chat_id: ENV['TELEGRAM_CHAT_ID'],
            parse_mode: 'HTML',
            text: telegram_content,
            disable_notification: false,
            reply_markup: reply_markup,
          )['message_id']
          
        when 1
          case media[0]['type']
          when 'image'
            sent_message_id = @telegram.send_photo(
              chat_id: ENV['TELEGRAM_CHAT_ID'],
              parse_mode: 'HTML',
              caption: telegram_content,
              photo: media[0]['url'],
              has_spoiler: hide_images,
              disable_notification: false,
              reply_markup: reply_markup,
            )['message_id']
          when 'video', 'gifv'
            sent_message_id = @telegram.send_video(
              chat_id: ENV['TELEGRAM_CHAT_ID'],
              parse_mode: 'HTML',
              caption: telegram_content,
              video: media[0]['url'],
              has_spoiler: hide_images,
              disable_notification: false,
              reply_markup: reply_markup,
            )['message_id']
          end

        else
          # Send multi-media status as a media group plus a regular message
          @telegram.send_media_group(
            chat_id: ENV['TELEGRAM_CHAT_ID'],
            media: media.map{|medium|
              obj = {media: medium['url'], has_spoiler: hide_images}
              case medium['type']
              when 'image'
                obj.merge({type: 'photo'})
              when 'video', 'gifv'
                obj.merge({type: 'video'})
              end
            },
            disable_notification: true,
          )
          sent_message_id = @telegram.send_message(
            chat_id: ENV['TELEGRAM_CHAT_ID'],
            parse_mode: 'HTML',
            text: telegram_content,
            disable_notification: false,
            reply_markup: reply_markup,
          )['message_id']
        end

        # Forward messages
        (ENV['TELEGRAM_FORWARD_CHAT_IDS'] || '').split(',').each do |forward_chat_id|
          # Telegram does not allow forwarding media groups, so we resend them to the forwarded location
          if media.length > 1
            @telegram.send_media_group(
              chat_id: forward_chat_id,
              media: media.map{|medium|
                obj = {media: medium['url'], has_spoiler: hide_images}
                case medium['type']
                when 'image'
                  obj.merge({type: 'photo'})
                when 'video', 'gifv'
                  obj.merge({type: 'video'})
                end
              },
              disable_notification: true,
            )
          end

          @telegram.forward_message(
            chat_id: forward_chat_id,
            from_chat_id: ENV['TELEGRAM_CHAT_ID'],
            message_id: sent_message_id,
            disable_notification: false,
          )
        end
      end

      # Update last synced status time
      self.last_synced_status_time = status['created_at']
    end
  end

  def html_escape(text)
    text
      .gsub('&', '&amp;')
      .gsub('<', '&lt;')
      .gsub('>', '&gt;')
      .gsub('"', '&quot;')
  end
end