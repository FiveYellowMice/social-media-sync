class SocialMediaSync
  class CLIParser
    def self.parse
      # Extract subcommand and leave the rest of ARGV intact
      command = 'sync'
      if ARGV.length >= 1
        case ARGV[0]
        when 'twitter-auth'
          command = 'twitter-auth'
          ARGV.shift
        when 'sync'
          ARGV.shift
        end
      end
      command
    end
  end
end