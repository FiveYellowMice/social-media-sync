require 'dotenv'

class SocialMediaSync
  class ConfigLoader
    ENV_VARS = {
      'STATE_DIR' => true,

      'MASTODON_URL' => true,
      'MASTODON_USERNAME' => true,
      'MASTODON_CLIENT_ID' => true,
      'MASTODON_CLIENT_SECRET' => true,
      'MASTODON_ACCESS_TOKEN' => true,

      'TELEGRAM_TOKEN' => true,
      'TELEGRAM_USERNAME' => true,
      'TELEGRAM_CHAT_ID' => true,
      'TELEGRAM_FORWARD_CHAT_IDS' => false,
      'TELEGRAM_DISCUSSION_INVITE_LINK' => false,

      'TWITTER_USERNAME' => true,
      'TWITTER_CLIENT_ID' => true,
      'TWITTER_CLIENT_SECRET' => true,
    }

    def self.load

      # Load from .env file
      Dotenv.load
      # Load from CLI
      ARGV.each do |arg|
        entry = arg.split('=')
        if entry.length == 2
          ENV[entry[0]] = entry[1]
        else
          raise "Invalid argument: #{arg}"
        end
      end

      # Check for required variables
      ENV_VARS.each do |key, required|
        if required && (ENV[key].nil? || ENV[key].strip.empty?)
          raise "Missing required config: #{key}"
        end
      end

      nil

    end
  end
end