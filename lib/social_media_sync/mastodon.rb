require 'faraday'
require 'json'

class SocialMediaSync
  class Mastodon

    def initialize
      @http_client = Faraday.new(
        url: ENV['MASTODON_URL'],
        headers: {
          'Accepts' => 'application/json',
          'Authorization' => "Bearer #{ENV['MASTODON_ACCESS_TOKEN']}",
        },
      )
    end

    def get(path, params)
      response = @http_client.get(path, params)
      if response.status != 200
        raise Error, "Mastodon API returned status #{response.status}"
      end
      JSON.parse(response.body)
    end

    def post(path, params)
      response = @http_client.post(path, params)
      if response.status != 200
        raise Error, "Mastodon API returned status #{response.status}"
      end
      JSON.parse(response.body)
    end

    class Error < RuntimeError
    end

  end
end