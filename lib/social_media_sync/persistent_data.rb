class SocialMediaSync

  # Mixin to add persistent data accessors to a class
  module PersistentData

    def self.included(base)
      base.extend(ClassMethods)
    end

    module ClassMethods
      private

      def persistent_data_reader(key, filename, default_value = nil, &decoder)
        define_method(key) do
          file_content = begin
            File.read("#{ENV['STATE_DIR']}/#{filename}")
          rescue Errno::ENOENT
            return default_value
          end

          if decoder
            decoder.call(file_content)
          else
            file_content
          end
        end
      end

      def persistent_data_writer(key, filename, &encoder)
        define_method("#{key}=") do |value|
          if encoder
            value = encoder.call(value)
          end
          File.write("#{ENV['STATE_DIR']}/#{filename}", value)
        end
      end

      def persistent_data_accessor(key, filename, default_value, decoder, encoder)
        persistent_data_reader(key, filename, default_value, &decoder)
        persistent_data_writer(key, filename, &encoder)
      end

    end

  end
end