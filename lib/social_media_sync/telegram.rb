require 'faraday'
require 'json'

class SocialMediaSync
  class Telegram

    def initialize
      @http_client = Faraday.new(
        url: "https://api.telegram.org/bot#{ENV['TELEGRAM_TOKEN']}/",
        headers: {'Content-Type' => 'application/json'},
      )
    end

    def make_api_request(method, data)
      response = @http_client.post(method, data.to_json)
      begin
        response_json = JSON.parse(response.body)
      rescue JSON::ParserError
        raise Error, "Telegram API returned unparsable JSON, HTTP status #{response.status}"
      end
      if !response_json['result']
        raise Error, "Telegram API returned error (HTTP status #{response.status}): #{response_json['description']}"
      end
      response_json['result']
    end

    def method_missing(method, *args)
      if args.length != 1
        raise ArgumentError, "wrong number of arguments (given #{args.length}, expected 1)"
      end
      method = method.to_s.gsub(/_[a-z]/){|a| a[1].upcase }
      make_api_request(method, args[0])
    end

    class Error < RuntimeError
    end

  end
end
