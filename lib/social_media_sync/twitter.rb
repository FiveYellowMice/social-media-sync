require 'faraday'
require 'json'
require 'yaml'
require 'uri'
require 'securerandom'
require 'base64'

class SocialMediaSync
  class Twitter

    include PersistentData
    persistent_data_accessor :auth_tokens, "twitter_auth.yaml", {}, YAML.method(:load), YAML.method(:dump)

    def initialize
      @http_client = Faraday.new("https://api.twitter.com/2/") do |conn|
        conn.headers = {
          'Accepts' => 'application/json',
        }
        conn.request :json
      end
    end

    [:get, :post].each do |method|
      define_method(method) do |path, params|
        if auth_tokens[:access_token].nil? || auth_tokens[:access_token].empty?
          raise Error, "access token not found"
        end

        retres = 0
        response = nil
        loop do
          response = @http_client.send(method, path, params, {'Authorization' => "Bearer #{auth_tokens[:access_token]}"})
          if response.status == 401
            if retres < 1
              refresh_tokens
              retres += 1
              next
            else
              raise Error, "Twitter API authorization error: " + response.body
            end
          end
          break
        end

        if ![200, 201].include?(response.status)
          raise Error, "Twitter API returned status #{response.status} " + response.body
        end
        JSON.parse(response.body)
      end
    end

    def initial_authorize(&block)
      state = SecureRandom.hex(32)
      code_verifier = SecureRandom.hex(32)
      code_challenge = Base64.urlsafe_encode64(Digest::SHA256.digest(code_verifier)).gsub(/=+$/, '')

      auth_code = block.call("https://twitter.com/i/oauth2/authorize?" + URI.encode_www_form({
        response_type: "code",
        client_id: ENV['TWITTER_CLIENT_ID'],
        scope: "tweet.read tweet.write users.read offline.access",
        redirect_uri: "https://util.fiveyellowmice.com/httpecho/",
        state: state,
        code_challenge: code_challenge,
        code_challenge_method: "S256",
      }))
      
      response = @http_client.post("oauth2/token", {
        grant_type: "authorization_code",
        code: auth_code,
        redirect_uri: "https://util.fiveyellowmice.com/httpecho/",
        code_verifier: code_verifier,
      }, {
        'Authorization' => app_auth_header,
      })
      response_obj = JSON.parse(response.body)
      if response_obj['access_token'].nil? || response_obj['refresh_token'].nil?
        raise Error, "Twitter API authorization error: " + response.body
      end
      self.auth_tokens = {
        access_token: response_obj['access_token'],
        refresh_token: response_obj['refresh_token'],
      }
    end

    def refresh_tokens
      if auth_tokens[:refresh_token].nil? || auth_tokens[:refresh_token].empty?
        raise Error, "refresh token not found"
      end
      response = @http_client.post("oauth2/token", {
        grant_type: "refresh_token",
        refresh_token: auth_tokens[:refresh_token],
      }, {
        'Authorization' => app_auth_header,
      })
      response_obj = JSON.parse(response.body)
      if response_obj['access_token'].nil? || response_obj['refresh_token'].nil?
        raise Error, "Twitter API token refresh error: " + response.body
      end
      self.auth_tokens = {
        access_token: response_obj['access_token'],
        refresh_token: response_obj['refresh_token'],
      }
    end

    private

    def app_auth_header
      'Basic ' + Base64.strict_encode64("#{ENV['TWITTER_CLIENT_ID']}:#{ENV['TWITTER_CLIENT_SECRET']}")
    end

    class Error < RuntimeError
    end

  end
end