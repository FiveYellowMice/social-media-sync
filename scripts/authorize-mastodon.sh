#!/bin/bash

app_auth=$(curl -X POST \
  -F 'client_name=Social Media Sync' \
  -F 'redirect_uris=urn:ietf:wg:oauth:2.0:oob' \
  -F 'scopes=read write' \
  -F 'website=https://gitlab.com/FiveYellowMice/social-media-sync' \
  "$1/api/v1/apps")

client_id=$(jq -r .client_id <<< "$app_auth")
client_secret=$(jq -r .client_secret <<< "$app_auth")

echo client_id="$client_id"
echo client_secret="$client_secret"

echo "Open $1/oauth/authorize?client_id=$client_id&scope=read+write&redirect_uri=urn:ietf:wg:oauth:2.0:oob&response_type=code"

echo -n "Paste code: "
read code

user_auth=$(curl -X POST \
  -F "client_id=$client_id" \
  -F "client_secret=$client_secret" \
  -F 'redirect_uri=urn:ietf:wg:oauth:2.0:oob' \
  -F 'grant_type=authorization_code' \
  -F "code=$code" \
  -F 'scope=read write' \
  "$1/oauth/token")

echo access_token=$(jq -r .access_token <<< "$user_auth")
