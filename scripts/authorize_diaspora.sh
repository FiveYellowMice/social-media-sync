#!/bin/bash

app_auth=$(curl -X POST \
  -H "Content-Type: application/json; charset=utf-8" \
  -d '{
    "client_name": "Social Media Sync",
    "client_uri": "https://gitlab.com/FiveYellowMice/social-media-sync",
    "redirect_uris": ["https://example.com/"]
  }' \
  "$1/api/openid_connect/clients")

client_id=$(jq -r .client_id <<< "$app_auth")
client_secret=$(jq -r .client_secret <<< "$app_auth")

echo client_id="$client_id"
echo client_secret="$client_secret"

echo "Open $1/api/openid_connect/authorizations/new?client_id=$client_id&scope=openid+read+write&redirect_uri=https%3A%2F%2Fexample.com%2F&response_type=code"

echo -n "Paste code: "
read code

user_auth=$(curl -X POST \
  -F "client_id=$client_id" \
  -F "client_secret=$client_secret" \
  -F "redirect_uri=https://example.com/" \
  -F "grant_type=authorization_code" \
  -F "code=$code" \
  "$1/api/openid_connect/access_tokens")

echo access_token=$(jq -r .access_token <<< "$user_auth")
